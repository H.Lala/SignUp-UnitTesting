FROM alpine:3.11.3
RUN echo "Hello from unit testing"
RUN apk add --no-cache openjdk11
COPY /build/libs/unitTesting-0.0.1-SNAPSHOT.jar /app/
WORKDIR /app/
CMD ["java", "-jar", "/app/unitTesting-0.0.1-SNAPSHOT.jar"]