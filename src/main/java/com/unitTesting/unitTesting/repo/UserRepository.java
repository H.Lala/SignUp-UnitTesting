package com.unitTesting.unitTesting.repo;

import com.unitTesting.unitTesting.entity.Users;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<Users,Long> {

    Users findByEmail(String email);
}
