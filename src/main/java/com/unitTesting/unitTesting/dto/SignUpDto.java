package com.unitTesting.unitTesting.dto;


import lombok.Data;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;

@Data
public class SignUpDto {

    @NotNull
    private String email;

    @NotNull
    private String password;

    @NotNull
    private String passwordConf;

    private Boolean privacyPolicyAccepted;

}
