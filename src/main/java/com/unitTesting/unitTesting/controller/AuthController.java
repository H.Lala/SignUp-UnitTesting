package com.unitTesting.unitTesting.controller;


import com.unitTesting.unitTesting.dto.SignUpDto;
import com.unitTesting.unitTesting.entity.Users;
import com.unitTesting.unitTesting.services.AuthService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/auth")
public class AuthController {

    private final AuthService authService;

    @PostMapping("/sign-up")
    public SignUpDto signUp(@RequestBody @Valid SignUpDto signUpDto){
        return authService.signUp(signUpDto);
    }
}
