package com.unitTesting.unitTesting.exception;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
public class PasswordDoNotMatchException extends RuntimeException {

    private String path;

    private final static String MESSAGE = "Password do not match";

    public PasswordDoNotMatchException() {
        super(MESSAGE);
    }
}
