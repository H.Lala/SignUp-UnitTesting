package com.unitTesting.unitTesting.exception;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class InvalidEmailException extends RuntimeException{
    private String path;

    private final static String MESSAGE = "Email must be in valid form";

//    public InvalidEmailException() {
//        super(MESSAGE);
//    }
}
