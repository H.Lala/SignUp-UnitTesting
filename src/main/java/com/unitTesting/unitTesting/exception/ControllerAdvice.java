package com.unitTesting.unitTesting.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

@org.springframework.web.bind.annotation.ControllerAdvice
public class ControllerAdvice {
    @ExceptionHandler(PasswordDoNotMatchException.class)
    public ResponseEntity<ReturnFormatExceptions> handlePasswordNotMatching(PasswordDoNotMatchException passwordDoNotMatchException) {

        ReturnFormatExceptions returnFormatExceptions = new ReturnFormatExceptions();
        returnFormatExceptions.setTimestamp(LocalDateTime.now());
        returnFormatExceptions.setStatus(HttpStatus.BAD_REQUEST.value());
        returnFormatExceptions.setError(HttpStatus.BAD_REQUEST);
        returnFormatExceptions.setMessage("Argument validation failed");

        List<Map<String, String>> errors = List.of(
                Map.of("property", "password",
                        "message", "Password do not match")
        );
        returnFormatExceptions.setErrors(errors);
        returnFormatExceptions.setPath(passwordDoNotMatchException.getPath());

        return new ResponseEntity<>(returnFormatExceptions, returnFormatExceptions.getError());

    }

    @ExceptionHandler(PasswordRequirementsException.class)
    public ResponseEntity<ReturnFormatExceptions> handlePasswordRequirement(PasswordRequirementsException passwordRequirementsException) {

        ReturnFormatExceptions returnFormatExceptions = new ReturnFormatExceptions();
        returnFormatExceptions.setTimestamp(LocalDateTime.now());
        returnFormatExceptions.setStatus(HttpStatus.BAD_REQUEST.value());
        returnFormatExceptions.setError(HttpStatus.BAD_REQUEST);
        returnFormatExceptions.setMessage("Argument validation failed");

        List<Map<String, String>> errors = List.of(
                Map.of("property", "Password",
                        "message", "Password's length must be between in 8 and 30")
        );
        returnFormatExceptions.setErrors(errors);
        returnFormatExceptions.setPath(passwordRequirementsException.getPath());

        return new ResponseEntity<>(returnFormatExceptions, returnFormatExceptions.getError());

    }
    @ExceptionHandler(InvalidEmailException.class)
    public ResponseEntity<ReturnFormatExceptions> handleInvalidEmail(InvalidEmailException invalidEmailException) {

        ReturnFormatExceptions returnFormatExceptions = new ReturnFormatExceptions();
        returnFormatExceptions.setTimestamp(LocalDateTime.now());
        returnFormatExceptions.setStatus(HttpStatus.BAD_REQUEST.value());
        returnFormatExceptions.setError(HttpStatus.BAD_REQUEST);
        returnFormatExceptions.setMessage("Argument validation failed");

        List<Map<String, String>> errors = List.of(
                Map.of("property", "Email",
                        "message", "Email must be in valid form")
        );
        returnFormatExceptions.setErrors(errors);
        returnFormatExceptions.setPath(invalidEmailException.getPath());

        return new ResponseEntity<>(returnFormatExceptions, returnFormatExceptions.getError());

    }

    @ExceptionHandler(InputCanNotBeEmptyException.class)
    public ResponseEntity<ReturnFormatExceptions> handleEmptyInput(InputCanNotBeEmptyException inputCanNotBeEmptyException) {

        ReturnFormatExceptions returnFormatExceptions = new ReturnFormatExceptions();
        returnFormatExceptions.setTimestamp(LocalDateTime.now());
        returnFormatExceptions.setStatus(HttpStatus.BAD_REQUEST.value());
        returnFormatExceptions.setError(HttpStatus.BAD_REQUEST);
        returnFormatExceptions.setMessage("Argument validation failed");

        List<Map<String, String>> errors = List.of(
                Map.of("property", "Input",
                        "message", "Input can't be empty")
        );
        returnFormatExceptions.setErrors(errors);
        returnFormatExceptions.setPath(inputCanNotBeEmptyException.getPath());

        return new ResponseEntity<>(returnFormatExceptions, returnFormatExceptions.getError());

    }

    @ExceptionHandler(EmailAlreadyUsedException.class)
    public ResponseEntity<ReturnFormatExceptions> handleAlreadyUsedEmail(EmailAlreadyUsedException emailAlreadyUsedException) {

        ReturnFormatExceptions returnFormatExceptions = new ReturnFormatExceptions();
        returnFormatExceptions.setTimestamp(LocalDateTime.now());
        returnFormatExceptions.setStatus(HttpStatus.BAD_REQUEST.value());
        returnFormatExceptions.setError(HttpStatus.BAD_REQUEST);
        returnFormatExceptions.setMessage("Argument validation failed");

        List<Map<String, String>> errors = List.of(
                Map.of("property", "Email",
                        "message", "Email already used")
        );
        returnFormatExceptions.setErrors(errors);
        returnFormatExceptions.setPath(emailAlreadyUsedException.getPath());

        return new ResponseEntity<>(returnFormatExceptions, returnFormatExceptions.getError());

    }
}
