package com.unitTesting.unitTesting.exception;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.http.HttpStatus;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ReturnFormatExceptions {
    private LocalDateTime timestamp;
    private int status;
    private HttpStatus error;
    private String message;
    List<Map<String,String>> errors;
    private String path;
}


//        "timestamp": "2021-10-26T12:28:52.236+00:00",
//        "status": 400,
//        "error": "Bad Request",
//        "message": "Argument validation failed",
//        "errors": [
//        {
//        "property": "email",
//        "message": "must be a well-formed email address"
//        }
//        ],
//        "path": "/v1/auth/sign-up"
//        }

