package com.unitTesting.unitTesting.exception;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class InputCanNotBeEmptyException extends RuntimeException{
    private String inputValue;
    private String path;

}
