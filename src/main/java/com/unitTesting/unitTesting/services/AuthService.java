package com.unitTesting.unitTesting.services;

import com.unitTesting.unitTesting.dto.SignUpDto;
import com.unitTesting.unitTesting.entity.Users;
import com.unitTesting.unitTesting.exception.*;
import com.unitTesting.unitTesting.repo.UserRepository;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class AuthService {

    private final UserRepository userRepository;
    private ModelMapper modelMapper;
    private String path ="/v1/auth/sign-up";

    public SignUpDto signUp(SignUpDto signUpDto) {

        //Checking empty inputs
        if (signUpDto.getEmail() == null || signUpDto.getEmail().isBlank()) {
            throw new InputCanNotBeEmptyException(path, "Email");
        } else if (signUpDto.getPassword() == null || signUpDto.getPassword().isBlank()) {
            throw new InputCanNotBeEmptyException(path, "Password");
        } else if (signUpDto.getPasswordConf() == null || signUpDto.getPasswordConf().isBlank()) {
            throw new InputCanNotBeEmptyException(path, "Password Confirm");
        } else if (signUpDto.getPrivacyPolicyAccepted() == null) {
            throw new InputCanNotBeEmptyException(path, "Privacy Policy");
        }

        //Checking password and passwordConf
        if (!signUpDto.getPassword().equals(signUpDto.getPasswordConf())) {
            throw new PasswordDoNotMatchException(path);
        }

        //Checking password requirements
        if (signUpDto.getPassword().length() < 8 || signUpDto.getPassword().length() > 30) {
            throw new PasswordRequirementsException(path);
        }

        //Checking email format
        if (!signUpDto.getEmail().contains("@")) {
            throw new InvalidEmailException(path);
        }
        //Checking email already in used or not
        if (userRepository.findAll().stream().map(users -> users.getEmail()).
                anyMatch(mail -> mail.equals(signUpDto.getEmail()))) {
            throw new EmailAlreadyUsedException(path);
        }

//        SignUpDto signUpUser = new SignUpDto();
//        signUpUser.setEmail(signUpDto.getEmail());
//        signUpUser.setPassword(signUpDto.getPassword());
//        signUpUser.setPasswordConf(signUpDto.getPasswordConf());
//        signUpUser.setPrivacyPolicyAccepted(signUpDto.getPrivacyPolicyAccepted());
            Users user = new Users();
            userRepository.save(modelMapper.map(signUpDto, Users.class));
        return signUpDto;
    }
}
