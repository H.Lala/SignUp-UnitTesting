package com.unitTesting.unitTesting.services;

import com.unitTesting.unitTesting.dto.SignUpDto;
import com.unitTesting.unitTesting.entity.Users;
import com.unitTesting.unitTesting.exception.*;
import com.unitTesting.unitTesting.repo.UserRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.ModelMapper;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class AuthServiceTest {

    private  AuthService authService;

    @Mock
    private UserRepository userRepository;

    @BeforeEach
    void before(){
        authService = new AuthService(userRepository);
    }

    @Test
    void whenPasswordDoNotMatchThenException(){
        //Arrange
        SignUpDto signUpDto = new SignUpDto();
        signUpDto.setEmail("test@mail.ru");
        signUpDto.setPassword("1234");
        signUpDto.setPasswordConf("4567");
        signUpDto.setPrivacyPolicyAccepted(true);

        //Act & Arrange
            assertThatThrownBy(()->authService.signUp(signUpDto)).isInstanceOf(PasswordDoNotMatchException.class);
    }

    @Test
    void whenPasswordRequirementNotCompletedThenException(){
        //Arrange
        SignUpDto signUpDto = new SignUpDto();
        signUpDto.setEmail("test@mail.ru");
        signUpDto.setPassword("1234");
        signUpDto.setPasswordConf("1234");
        signUpDto.setPrivacyPolicyAccepted(true);

        //Act & Arrange
        assertThatThrownBy(()->authService.signUp(signUpDto)).isInstanceOf(PasswordRequirementsException.class);
    }

    @Test
    void whenInvalidEmailThenException(){
        //Arrange
        SignUpDto signUpDto = new SignUpDto();
        signUpDto.setEmail("test.bk.ru");
        signUpDto.setPassword("123456789");
        signUpDto.setPasswordConf("123456789");
        signUpDto.setPrivacyPolicyAccepted(true);


        //Act & Arrange
        assertThatThrownBy(()->authService.signUp(signUpDto)).isInstanceOf(InvalidEmailException.class);
    }

    @Test
    void givenEmptyInputThenException(){
        //Arrange
        SignUpDto signUpDto = new SignUpDto();
        signUpDto.setEmail(null);
        signUpDto.setPassword("123456789");
        signUpDto.setPasswordConf("123456789");
        signUpDto.setPrivacyPolicyAccepted(true);

        //Act & Arrange
        assertThatThrownBy(()->authService.signUp(signUpDto)).isInstanceOf(InputCanNotBeEmptyException.class);


        //Arrange
        signUpDto.setEmail("test@mail.ru");
        signUpDto.setPassword(null);
        signUpDto.setPasswordConf("123456789");
        signUpDto.setPrivacyPolicyAccepted(true);

        //Act & Arrange
        assertThatThrownBy(()->authService.signUp(signUpDto)).isInstanceOf(InputCanNotBeEmptyException.class);

        //Arrange
        signUpDto.setEmail("test@mail.ru");
        signUpDto.setPassword("123456789");
        signUpDto.setPasswordConf(null);
        signUpDto.setPrivacyPolicyAccepted(true);

        //Act & Arrange
        assertThatThrownBy(()->authService.signUp(signUpDto)).isInstanceOf(InputCanNotBeEmptyException.class);

        //Arrange
        signUpDto.setEmail("test@mail.ru");
        signUpDto.setPassword("123456789");
        signUpDto.setPasswordConf("123456789");
        signUpDto.setPrivacyPolicyAccepted(null);

        //Act & Arrange
        assertThatThrownBy(()->authService.signUp(signUpDto)).isInstanceOf(InputCanNotBeEmptyException.class);
    }
    @Test
    void whenEmailAlreadyUsedThenException(){
        //Arrange
        SignUpDto signUpDto = new SignUpDto();
        signUpDto.setEmail("test@bk.ru");
        signUpDto.setPassword("123456789");
        signUpDto.setPasswordConf("123456789");
        signUpDto.setPrivacyPolicyAccepted(true);

        var user =
                List.of(new Users(1L,"test@bk.ru","123456789"));
        when(userRepository.findAll()).thenReturn(user);

        //Act & Arrange
        assertThatThrownBy(()->authService.signUp(signUpDto)).isInstanceOf(EmailAlreadyUsedException.class);
    }
}