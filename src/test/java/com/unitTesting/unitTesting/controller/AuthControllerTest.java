package com.unitTesting.unitTesting.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.unitTesting.unitTesting.dto.SignUpDto;
import com.unitTesting.unitTesting.exception.*;
import com.unitTesting.unitTesting.services.AuthService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import static org.hamcrest.Matchers.in;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;
import static org.hamcrest.Matchers.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;


@AutoConfigureMockMvc
@ExtendWith(SpringExtension.class)
@WebMvcTest(AuthController.class)
class AuthControllerTest {

    @MockBean
    private AuthService authService;

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    private String path = "/v1/auth/sign-up";

    @Test
    void whenEmptyInputThenBadRequest() throws Exception {

        //Arrange
        SignUpDto signUpDto = new SignUpDto();
        signUpDto.setEmail(null);
        signUpDto.setPassword("12345678");
        signUpDto.setPasswordConf("12345678");
        signUpDto.setPrivacyPolicyAccepted(true);

        InputCanNotBeEmptyException inputException = new InputCanNotBeEmptyException(path, "email");
        when(authService.signUp(signUpDto)).thenThrow(inputException);

        //Act
        mockMvc.perform(
                post("/v1/auth/sign-up")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(signUpDto))
        ).andExpect(status().isBadRequest());
    }

    @Test
    void whenPasswordDoNotMatchThenBadRequest() throws Exception {

        //Arrange
        SignUpDto signUpDto = new SignUpDto();
        signUpDto.setEmail("test@bk.ru");
        signUpDto.setPassword("12345");
        signUpDto.setPasswordConf("1234");
        signUpDto.setPrivacyPolicyAccepted(true);

        PasswordDoNotMatchException passwordDoNotMatchException = new PasswordDoNotMatchException(path);

        when(authService.signUp(signUpDto)).thenThrow(passwordDoNotMatchException);

        //Act
        mockMvc.perform(
                post("/v1/auth/sign-up")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(signUpDto))
        ).andExpect(status().isBadRequest())
                .andExpect(content().string(containsString("Password")))
                .andExpect(content().string(containsString("Password do not match")));
    }

    @Test
    void whenInvalidEmailThenBadRequest() throws Exception {

        //Arrange
        SignUpDto signUpDto = new SignUpDto();
        signUpDto.setEmail("test.bk.ru");
        signUpDto.setPassword("12345");
        signUpDto.setPasswordConf("12345");
        signUpDto.setPrivacyPolicyAccepted(true);

        InvalidEmailException invalidEmailException = new InvalidEmailException(path);

        when(authService.signUp(signUpDto)).thenThrow(invalidEmailException);

        //Act
        mockMvc.perform(
                post("/v1/auth/sign-up")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(signUpDto))
        ).andExpect(status().isBadRequest())
                .andExpect(content().string(containsString("Email")))
                .andExpect(content().string(containsString("Email must be in valid form")));
    }

    @Test
    void whenPasswordRequirementNotRightThenBadRequest() throws Exception {

        //Arrange
        SignUpDto signUpDto = new SignUpDto();
        signUpDto.setEmail("test@bk.ru");
        signUpDto.setPassword("1234");
        signUpDto.setPasswordConf("1234");
        signUpDto.setPrivacyPolicyAccepted(true);

        PasswordRequirementsException passwordRequirementsException = new PasswordRequirementsException(path);

        when(authService.signUp(signUpDto)).thenThrow(passwordRequirementsException);

        //Act
        mockMvc.perform(
                post("/v1/auth/sign-up")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(signUpDto))
        ).andExpect(status().isBadRequest())
                .andExpect(content().string(containsString("Password")))
                .andExpect(content().string(containsString("Password's length must be between in 8 and 30")));
    }
    @Test
    void whenGivenMailAlreadyUsedThenBadRequest() throws Exception {

        //Arrange
        SignUpDto signUpDto = new SignUpDto();
        signUpDto.setEmail("test@bk.ru");
        signUpDto.setPassword("123456789");
        signUpDto.setPasswordConf("123456789");
        signUpDto.setPrivacyPolicyAccepted(true);

        EmailAlreadyUsedException emailAlreadyUsedException = new EmailAlreadyUsedException(path);

        when(authService.signUp(signUpDto)).thenThrow(emailAlreadyUsedException);

        //Act
        mockMvc.perform(
                post("/v1/auth/sign-up")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(signUpDto))
        ).andExpect(status().isBadRequest())
                .andExpect(content().string(containsString("Email")))
                .andExpect(content().string(containsString("Email already used")));
    }

    @Test
    void whenValidDtoThenBadRequest() throws Exception {
        //Arrange
        SignUpDto signUpDto = new SignUpDto();
        signUpDto.setEmail("lola@gmail.com");
        signUpDto.setPassword("lola12345");
        signUpDto.setPasswordConf("lola12345");


        when(authService.signUp(signUpDto)).thenReturn(signUpDto);

        //Act
        mockMvc.perform(
                post("/v1/auth/sign-up")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(signUpDto))
        ).andExpect(status().isOk())
                .andExpect(content().json(objectMapper.writeValueAsString(signUpDto)));

    }

}
